# Ghasedak Ruby Tool 

This is a simple tool for accessing [Ghasedak's](https://ghasedak.io) REST API from a ruby/rails project. It'll be a ruby gem soon. 

## Creating an instance of the class 

```ruby 
sender = GhasedakApi.new(api_key, linenumber) 
``` 

Please consider if you defined an environment variable called `ghasedak_api`, it will find and use it very well. 

## Methods 

### send_simple_sms 

Parameters : `receptor` (a single string, phone number), `message` (a single string text)

```ruby 
sender = GhasedakApi.new(api_key, linenumber) # Leave empty if you have environment variable called ghasedak_key and if you didn't buy any dedicated linenumber. 

sent_message = sender.send_simple_sms("09370000000", "Hello, World!") 
``` 

### send_pair_sms 

Parameters : `receptors` (an arrya of numbers, each phone number must be a sting), `message` (a single string text) 

```ruby 
nums = ["09120000000", "09210000000", ... , "09370000000"] 
sender = GhasedakApi.new(api_key, linenumber) # Leave empty if you have environment variable called ghasedak_key and if you didn't buy any dedicated linenumber. 

sent_message = sender.send_pair_sms(nums, "Hello, World!") 
``` 